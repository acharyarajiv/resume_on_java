package com.mycompany;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import main.webapp.WebServer;

public class HelloWorld extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.getWriter().print("Hello from Java!\n");
    }
  
  public static void main(String[] args) throws Exception {
    WebServer webServer = WebServer.getInstance();
    webServer.setPort(System.getenv("PORT"));
    String webappDirLocation = "src/main/java/com/mycompany";
    webServer.setWebAppDirectoryLocation(webappDirLocation);

    //servlets for sms service
    webServer.addServlet("HelloWorld", "com.mycompany.HelloWorld");
    webServer.addServletMapping("/helloworld", "HelloWorld");
    webServer.start();
    
    
}
}