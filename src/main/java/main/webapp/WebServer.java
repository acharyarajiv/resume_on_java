/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.webapp;

import java.io.File;
import javax.servlet.ServletException;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.Wrapper;
import org.apache.catalina.startup.Tomcat;

/**
 *
 * @author Hari
 */
public class WebServer {
   // private static Logger logger = Logger.getLogger(WebServer.class);
    
    private static Tomcat tomcat = new Tomcat();
    private static WebServer webServer = new WebServer();
    private Context webServerContext;
    
    public static WebServer getInstance() {
      return webServer;  
    }//getInstance
    
    public void setPort(String webPort) {
        if(webPort == null || webPort.isEmpty()) {
            webPort = "8083";
        }
        tomcat.setPort(Integer.valueOf(webPort));
    }//setPort
    
    public void setWebAppDirectoryLocation(String webAppDirLocation) 
            throws ServletException {
        //logger.info("Configuring WebApp with basedir: " + new File("./" + webAppDirLocation).getAbsolutePath());

        webServerContext = tomcat.addWebapp("/", 
              new File(webAppDirLocation).getAbsolutePath());
        // Define DefaultServlet.
        Wrapper defaultServlet = webServerContext.createWrapper();
        defaultServlet.setName("default");
        defaultServlet.setServletClass("org.apache.catalina.servlets.DefaultServlet");
        defaultServlet.addInitParameter("debug", "0");
        defaultServlet.addInitParameter("listings", "false");
        defaultServlet.setLoadOnStartup(1);
       // ctx.addChild(defaultServlet);
        //tomcat.addServlet(ctx, "default", defaultServlet);
        //http://stackoverflow.com/questions/6349472/embedded-tomcat-not-serving-static-content
       // ctx.addServletMapping("/", "default");
  
    }
    
    public void addServlet(String servletName, String servletClass) {
      tomcat.addServlet(webServerContext, 
              servletName, servletClass).setLoadOnStartup(1); 
    }//addServlet
    
    public void addServletMapping(String path, String servletName) {
      webServerContext.addServletMapping(path, servletName);   
    }//addServletMapping
    
    public void start() throws LifecycleException {
      tomcat.start();
      tomcat.getServer().await();  
    }//start()
}